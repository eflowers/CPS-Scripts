Takes a list of urls and formats them.

Takes of list of urls from url_input.txt
Outputs them in formatted_urls.txt

Use formatting.pl to format the list of urls as seperated by a new line.
Use formatting_ironport.pl to format the list of urls as seperated by a comma and a space, as IronPort likes.