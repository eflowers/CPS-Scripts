# Perl script to automatically format a list of IPs to how IronPort likes them.
open(my $formatting, "<", 'url_list.txt') || die "Can't open url_list.txt: $!"; # Read from the file containing the list. Note that each listing must be on its own line.
open(my $output_file, ">", 'formatted_urls.txt') || die "Can't open formatted_urls.txt: $!"; # Output the formatted list into the specified file.
while (<$formatting>) {
    s/(\s+$|,|hxxp(s)?:\/\/|http(s)?:\/\/)//g; # Remove whitespace at the end of the listing, remove commas, and http and hxxp prefixes.
    s/(\ .\ |\[.\])/./g; # Fix the spaces around the dots. Ex: change www . google . com to www.google.com and fix the brackets around the dots, Ex: www[.]google[.]com
    s/(Sun|Mon|Tue|Wed|Thur|Fri|Sat).*//; # Remove the trailing dates from entries like clear-sky.tk,/nb4vervge,Mon Jul  4 08:20:02 2016
    print $output_file "$_, "; # Print out each listing with a comma and space seperating them.
}
print "Formatting complete.";

close $formatting;
close $output_file;
