#!perl
#TODO: Add checking database on whether they're blocked or not

use strict;
use warnings;
use LWP::Simple;
use WWW::Mechanize;
use HTML::Form;
use Excel::Writer::XLSX;
use Term::ProgressBar::Simple;

my @lookup_urls;
my @email_reputation;
my @web_reputation;
my @blacklist_spamcop;
my @blacklist_abuseat;
my @blacklist_pbl_spamhaus;
my @blacklist_sbl_spamhaus;
push @lookup_urls, 'Lookup URLs';
push @email_reputation, 'Email Reputation';
push @web_reputation, 'Web Reputation';
push @blacklist_spamcop, 'spamcop.net Blacklist';
push @blacklist_abuseat, 'abuseat.org Blacklist';
push @blacklist_pbl_spamhaus, 'pbl.spamhaus.org Blacklist';
push @blacklist_sbl_spamhaus, 'sbl.spamhaus.org Blacklist';
my $lookup;
my $emailrep;
my $rep;
my $bl_spamcop;
my $bl_abuseat;
my $bl_pbl_spamhaus;
my $bl_sbl_spamhaus;
my $workbook = Excel::Writer::XLSX->new('results.xlsx');

my @urlrange = do {
    open my $fh, "<", 'urls_to_lookup.txt' || die "Could not open urls_to_lookup.txt: $!";
    <$fh>;
};

my $urls_proccessed = 0;
my $number_of_urls = scalar @urlrange;
my $progress = Term::ProgressBar::Simple->new( $number_of_urls );
foreach my $url (@urlrange) {
    if ( $urls_proccessed > 100 ) {
        print "Sleeping for 10 minutes to avoid captcha";
        sleep ( 600 );
        $urls_proccessed = 0;
    }
    $progress->message("Looking up $url");
my $mech = WWW::Mechanize->new(); # Create a new WWW::Mechanize object. WWW::Mechanize is the Perl library that gets and formats the webpage.
    my $sburl = 'http://www.senderbase.org/lookup/?search_string=';
    my $sbsearch = $sburl . $url;
    $mech->agent( 'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_4; en-us) AppleWebKit/533.17.8 (KHTML, like Gecko) Version/5.0.1 Safari/533.17.8' );
    my $page = $mech->get($sbsearch); # Grab the page

    $mech->form_number(1); # Find form 1, which is the form that contains the button to accept the terms of service
    $mech->click_button( # Click the tos_accepted button
        name => 'tos_accepted'
    );

    open my $rawtext, ">", "rawtext_webpage.txt";
    $mech->dump_text($rawtext); # Dump text output of page to a file
    close $rawtext;

    open my $webpage, "<", "rawtext_webpage.txt";
    while(<$webpage>) {
        if(m/Email Reputation.*Read more. Good/) { # Email reputation lookup
            $emailrep = "Good";
        }
        elsif(m/Email Reputation.*Read more. Poor/) {
            $emailrep = "Poor";
        }
        elsif(m/Email Reputation.*Read more. Neutral/) {
            $emailrep = "Neutral";
        }
        else {
            $emailrep = "Not Found";
        }

        if(m/Web Reputation.*Read more. Good/) { # Web reputation lookup
            $rep = "Good";
        }
        elsif(m/Web Reputation.*Read more. Neutral/) {
            $rep = "Neutral";
        }
        elsif(m/Web Reputation.*Read more. Poor/) {
            $rep = "Poor";
        }
        elsif(m/Access Forbidden:*/) {
            print "\nFatal Error: Access Forbidden\n";
            last;
        }
        else {
            $rep = "Not Found";
        }

        if(m/bl.spamcop.netListed/) { # Blacklist lookups
            $bl_spamcop = "Listed";
        }
        elsif(m/bl.spamcop.netNot Listed/) {
            $bl_spamcop = "Not Listed";
        }
        else {
            $bl_spamcop = "No Information Found";
        }
        
        if(m/cbl.abuseat.orgListed/) {
            $bl_abuseat = "Listed"
        }
        elsif(m/cbl.abuseat.orgNot Listed/) {
            $bl_abuseat = "Not Listed"
        }
        else {
            $bl_abuseat = "No Information Found";
        }

        if(m/pbl.spamhaus.orgListed/) {
            $bl_pbl_spamhaus = "Listed"
        }
        elsif(m/pbl.spamhaus.orgNot Listed/) {
            $bl_pbl_spamhaus = "Not Listed"
        }
        else {
            $bl_pbl_spamhaus = "No Information Found";
        }

        if(m/sbl.spamhaus.orgListed/) {
            $bl_sbl_spamhaus = "Listed"
        }
        elsif(m/sbl.spamhaus.orgNot Listed/) {
            $bl_sbl_spamhaus = "Not Listed"
        }
        else {
            $bl_sbl_spamhaus = "No Information Found";
        }
    }

    chomp($url);
    push @lookup_urls, $url;
    push @email_reputation, $emailrep;
    push @web_reputation, $rep;
    push @blacklist_spamcop, $bl_spamcop;
    push @blacklist_abuseat, $bl_abuseat;
    push @blacklist_pbl_spamhaus, $bl_pbl_spamhaus;
    push @blacklist_sbl_spamhaus, $bl_sbl_spamhaus;
    $progress++;
    $urls_proccessed++;
}

my $worksheet = $workbook->add_worksheet();
my $heading = $workbook->add_format( align => 'center', bold => 1 );
$worksheet->set_column( 'A:A', 41 );
$worksheet->set_column( 'B:B', 16 );
$worksheet->set_column( 'C:C', 15 );
$worksheet->set_column( 'D:D', 20 );
$worksheet->set_column( 'E:E', 19 );
$worksheet->set_column( 'F:F', 25 );
$worksheet->set_column( 'G:G', 24 );
$worksheet->write( 0, 0, 'Lookup URLs', $heading );
$worksheet->write( 0, 1, 'Email Reputation', $heading );
$worksheet->write( 0, 2, 'Web Reputation', $heading );
$worksheet->write( 0, 3, 'spamcop.net Blacklist', $heading );
$worksheet->write( 0, 4, 'abuseat.org Blacklist', $heading );
$worksheet->write( 0, 5, 'pbl.spamhaus.org Blacklist', $heading );
$worksheet->write( 0, 6, 'sbl.spamhaus.org Blacklist', $heading );

my $counter = 1;
foreach(@lookup_urls) {
    $worksheet->write($counter, 0, $lookup_urls[$counter]);
    $worksheet->write($counter, 1, $email_reputation[$counter]);
    $worksheet->write($counter, 2, $web_reputation[$counter]);
    $worksheet->write($counter, 3, $blacklist_spamcop[$counter]);
    $worksheet->write($counter, 4, $blacklist_abuseat[$counter]);
    $worksheet->write($counter, 5, $blacklist_pbl_spamhaus[$counter]);
    $worksheet->write($counter, 6, $blacklist_sbl_spamhaus[$counter]);
    $counter++;
}
