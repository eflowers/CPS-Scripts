Lookup the Web Reputation of a List of URLs through Cisco's senderbase.org
This script needs the following perl modules installed:
-	LWP::Simple
-	WWW::Mechanize
-	HTML::Form
-	Excel::Writer::XLSX
-	Term::ProgressBar::Simple

If using Strawberry Perl in Windows, the following command will install them for you:
	cpan LWP::Simple WWW::Mechanize HTML::Form Excel::Writer::XLSX Term::ProgressBar::Simple

(The above command should work in most versions of *nix perl as well as most other versions of Windows perl, however it has only been tested on Strawberry Perl on Windows.)
	
Takes a list of ip addresses found in urls_to_lookup.txt as input. 
rawtext_webpage.txt is where the webpage is stored while being parsed.
Outputs the website looked-up along with the web reputation found for it in results.xlsx

Be careful not to feed it too many urls at once or Cisco gets mad and issues a captcha for all requests to senderbase, which breaks the script. 
A check for this was implemented; once a captcha is detected the script halts lookups, prints an error to STDOUT and writes any url and web reputation pairs it did find to results.xlsx
Past experience indicates that with the 10 minute waiting period after 100 lookups builtin to the script, 886 lookups can be successfully performed. However, your milage may vary.
Also, be aware that Cisco does not allow more than 1000 lookups per calendar day per IP or subnet.

Errors:
E:	Can't call method "add_worksheet" on an undefined value at .\sb_parse.pl line 58, <$urlrange> line 1.
A:	Perl can't write to the results.xlsx file because it is open or otherwise being used. Close the file and try again.

E:	Wide character in print at C:/Strawberry/perl/vendor/lib/WWW/Mechanize.pm line 2240, <$urlrange> line 1.
A:	Perl complains about the space in "Not Found" when outputting that a url isn't found on senderbase. Disregard error.